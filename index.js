var Ajv = require('ajv');
var ajv = new Ajv({ allErrors: true, loadSchema: loadSchema });
var request = require('request-promise');

var schema = {
    $id: "testSchema",
    type: "object",
    properties: {
        contributor: {
            "$ref": "http://schemas.nationella-riktlinjer.se/nrb-contributor.1.schema.json"
        }
    },
    required: ['contributor']
};

var data = {
    _v: 1,
    _schema: "nrb-concept.1.json",
    id: "262430bb-d728-4064-aa15-898649bccd37",
    slug: "test",
    language: "sv",
    term: "Building information modeling",
    abbrevation: "BIM",
    source: "Trafikverket",
    definition: "En samling av alla modeller i projektet",
    synonyms: ["test2"],
    links: [
        {
            "name": "whoop whoop",
            "uri": "http://google.com"
        }
    ],
    contributor: {
       "name": "Johan" 
    }
};

function loadSchema(uri) {
    return request
        .get(uri, { json: true })
        .then((schema) => schema);
}

ajv.compileAsync(schema)
    .then(function(validate) {
        validate(data);
        console.log(validate.errors);
    });